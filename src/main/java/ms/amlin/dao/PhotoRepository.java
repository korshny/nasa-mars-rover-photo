package ms.amlin.dao;

import ms.amlin.models.Photo;
import java.util.List;

/**
 * Photo DAO
 */
public interface PhotoRepository {

    public void save(List<Photo> photos);

    public List<Photo> getPhotos(String roverName, int sol);

    public List<Photo> allPhoto();
}

package ms.amlin.dao;

import ms.amlin.models.Photo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.*;
import org.springframework.stereotype.Repository;
import javax.imageio.ImageIO;
import java.io.ByteArrayOutputStream;
import java.sql.SQLException;
import java.util.List;

/**
 * DAO implementation
 */

@Repository
public class PhotoRepositoryImpl implements PhotoRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void save(List<Photo> photos) {
        jdbcTemplate.batchUpdate("INSERT into Photo(ID, Sol, IMAGECONTENT) values (?, ?, ?)",
                new BatchPreparedStatementSetter() {
                    @Override
                    public void setValues(java.sql.PreparedStatement preparedStatement, int i) throws SQLException {
                        Photo photo = photos.get(i);
                        preparedStatement.setLong(1, photo.getId());
                        preparedStatement.setInt(2, photo.getSol());
                        photo.getImage().ifPresent(img -> {
                            try {
                                ByteArrayOutputStream bao = new ByteArrayOutputStream();
                                ImageIO.write(img, "jpg", bao);
                                preparedStatement.setBytes(3, bao.toByteArray());
                            } catch (Exception e) { System.out.println(e.getMessage()); }
                        });
                    }

                    @Override
                    public int getBatchSize() {
                        return photos.size();
                    }
                });
    }

    public List<Photo> getPhotos(String roverName, int sol) {
        return jdbcTemplate.query(
                String.format("SELECT * FROM Photo WHERE Sol = %s", sol),
                (rs, rowNum) -> new Photo(
                        rs.getLong("ID"),
                        rs.getInt("Sol"),
                        rs.getBlob("IMAGECONTENT").getBytes(1, (int) rs.getBlob("IMAGECONTENT").length())
                )
        );
    }

    public List<Photo> allPhoto() {
        return jdbcTemplate.query(
                "SELECT * FROM Photo",
                (rs, rowNum) -> new Photo(
                        rs.getLong("ID"),
                        rs.getInt("Sol"),
                        rs.getBlob("IMAGECONTENT").getBytes(1, (int) rs.getBlob("IMAGECONTENT").length())
                )
        );
    }

}
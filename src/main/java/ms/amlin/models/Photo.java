package ms.amlin.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.awt.image.BufferedImage;
import java.util.Optional;

/**
 *
 */
@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@Table(name = "Photo")
public class Photo {

    @Id
    @Column(name = "ID")
    @JsonProperty("id")
    private long id;

    @Column(name = "Sol")
    @JsonProperty("sol")
    private int sol;

    @Transient
    @JsonProperty("img_src")
    private String img_src;

    @Transient
    private BufferedImage image;

    @Lob
    @Column(name = "IMAGECONTENT")
    private byte[] imageContent;

    @ManyToOne
    @JoinColumn(name="roverId", nullable = false)
    private Rover rover = new Rover(1, "curiosity"); //TODO: remove creation!

    public Photo() {}

    public Photo(long id, int sol, byte[] imageContent) {
        this.id = id;
        this.sol = sol;
        this.imageContent = imageContent;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getSol() {
        return sol;
    }

    public void setSol(int sol) {
        this.sol = sol;
    }

    public String getImg_src() {
        return img_src;
    }

    public void setImg_src(String img_src) {
        this.img_src = img_src;
    }

    public Optional<BufferedImage> getImage() { return Optional.ofNullable(image); }

    public void setImage(BufferedImage image) { this.image = image; }

    public Optional<byte[]> getImageContent() { return Optional.ofNullable(imageContent); }

    public void setImageContent(byte[] imageContent) { this.imageContent = imageContent; }

    @Override
    public String toString() {
        return "Photo{" +
                "id=" + id +
                ", sol=" + sol +
                '}';
    }
}

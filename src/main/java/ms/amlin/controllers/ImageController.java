package ms.amlin.controllers;

import ms.amlin.services.ImageService;
import ms.amlin.services.ViewCompositor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.io.IOException;

/**
 * Controller supply you with end-points to download images and after access it by sol and rover name or random select.
 */
@RestController
public class ImageController {

    @Autowired
    private ImageService imageService;

    @Autowired
    private ViewCompositor viewCompositor;

    @RequestMapping(value = "/nasa/mars/{roverName}/{sol}/images/download", method = RequestMethod.GET)
    public void download(@PathVariable String roverName, @PathVariable int sol) {
        imageService.downloadPhotos(roverName, sol);
    }

    @RequestMapping(value = "/nasa/mars/image/random", method = RequestMethod.GET, produces = "image/jpg")
    public byte[] random() {
        return imageService.randomImage().orElse(null);
    }

    @RequestMapping(value = "/nasa/mars/{roverName}/{sol}/images", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getTwoImages(@PathVariable String roverName, @PathVariable int sol) throws IOException {

        byte[] htmlData = viewCompositor.getHtmlData(imageService.images(roverName, sol));
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.TEXT_HTML);
        return new ResponseEntity<byte[]>(htmlData, headers, HttpStatus.OK);
    }

}

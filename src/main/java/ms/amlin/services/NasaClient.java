package ms.amlin.services;

import ms.amlin.models.Photo;
import ms.amlin.models.PhotoResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.List;
import java.util.Optional;

/**
 * Access nasa api to get images urls, and after download images by given urls.
 */
@Component
class NasaClient {

    @Value("${api.nasa.mars-photo.url}")
    private String urlWithParams;

    List<Photo> getPhotoInfo(String roverName, int sol) {
        RestTemplate restTemplate = new RestTemplate();
        PhotoResponse photos = restTemplate.getForObject(
                String.format(urlWithParams, roverName, sol),
                PhotoResponse.class);
        return photos.getPhotos();
    }

    Optional<BufferedImage> downloadImage(String url) {
        BufferedImage img = null;
        try {
            img = ImageIO.read(new URL(url));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return Optional.ofNullable(img);
    }

}

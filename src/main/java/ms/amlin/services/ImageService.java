package ms.amlin.services;

import ms.amlin.dao.PhotoRepository;
import ms.amlin.models.Photo;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Core service with built in nasa client component and photo repository
 */
@Service
public class ImageService {

    @Value("${image.height}")
    private Integer filterHeight;

    @Value("${image.width}")
    private Integer filterWidth;

    @Autowired
    private NasaClient nasaClient;

    @Autowired
    private PhotoRepository photoRepository;

    public void downloadPhotos(String roverName, int sol) {

        List<Photo> photos = nasaClient.getPhotoInfo(roverName, sol);
        photos.forEach(
                photo -> nasaClient.downloadImage(photo.getImg_src()).ifPresent(photo::setImage)
        );

        System.out.println("Downloaded: " + photos.size());
        List<Photo> filtered = filterGrayOrSmall(photos);
        System.out.println("Filtered: " + filtered.size());

        photoRepository.save(photos);//filterGrayOrSmall(photos));
    }

    public List<Byte[]> images(String roverName, int sol) {

        return photoRepository.getPhotos(roverName, sol).stream()
                .filter(photo -> photo.getImageContent().isPresent())
                .map(photo -> photo.getImageContent().get())
                .map(ArrayUtils :: toObject)
                .collect(Collectors.toList());
    }

    public Optional<byte[]> randomImage() {
        List<Photo> all = photoRepository.allPhoto();
        if (!all.isEmpty()) {
            Photo photo = all.get(new Random().nextInt(all.size() - 1));
            return photo.getImageContent();
        }
        else return Optional.empty();
    }

    private List<Photo> filterGrayOrSmall(List<Photo> photos) {

        return photos.stream().filter(photo -> photo.getImage().isPresent()
                && photo.getImage().get().getHeight() > filterHeight
                && photo.getImage().get().getWidth() > filterWidth
//                && photo.getImage().get().getColorModel().getNumColorComponents() != 1
        ).collect(Collectors.toList());
    }


}

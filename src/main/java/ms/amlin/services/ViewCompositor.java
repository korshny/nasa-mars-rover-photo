package ms.amlin.services;

import org.apache.commons.lang.ArrayUtils;
import org.apache.tomcat.util.codec.binary.Base64;
import org.apache.tomcat.util.codec.binary.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Compose bytes input for simple html page to present images
 */
@Service
public class ViewCompositor {

    public byte[] getHtmlData(List<Byte[]> imagesData){
        StringBuilder htmlContent = new StringBuilder();
        imagesData.forEach(img ->
                htmlContent.append(String.format("<img src='data:image/jpeg;base64, %s'/>",
                        StringUtils.newStringUtf8(Base64.encodeBase64(ArrayUtils.toPrimitive(img)))))
        );
        return htmlContent.toString().getBytes();
    }

}
